---
title: Merge Requests - January 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/01/
---

#### Week 01

- **Kate - [Add a toolbar for keyboard macros plugin.](https://invent.kde.org/utilities/kate/-/merge_requests/1054)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 2 days.

- **Kate - [Convert header guards to #pragma once](https://invent.kde.org/utilities/kate/-/merge_requests/1055)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **Kate - [Improvements around file history and commit view](https://invent.kde.org/utilities/kate/-/merge_requests/1058)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix showing diff for new and deleted files](https://invent.kde.org/utilities/kate/-/merge_requests/1059)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix widget closing and activation](https://invent.kde.org/utilities/kate/-/merge_requests/1057)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add missing include](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/462)**<br />
Request authored by [David Edmundson](https://invent.kde.org/davidedmundson) and merged at creation day.

#### Week 52

- **Kate - [lsp: Downgrade window/logMessage to MessageType::Log](https://invent.kde.org/utilities/kate/-/merge_requests/1052)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

