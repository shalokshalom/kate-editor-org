---
title: Merge Requests - May 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/05/
---

#### Week 22

- **Kate - [Fix &quot;Run Current Document&quot; so that it uses shebang or correct file path](https://invent.kde.org/utilities/kate/-/merge_requests/67)**<br />
Request authored by [Marcello Massaro](https://invent.kde.org/mmassaro) and merged after 99 days.

- **Kate - [Preview addon: Better update delay logic.](https://invent.kde.org/utilities/kate/-/merge_requests/85)**<br />
Request authored by [aa bb](https://invent.kde.org/aabb) and merged after 6 days.

- **Kate - [session: Sort session list by name again](https://invent.kde.org/utilities/kate/-/merge_requests/86)**<br />
Request authored by [Alex Hermann](https://invent.kde.org/alexhe) and merged after one day.

- **KTextEditor - [Add .diff to the file-changed-diff to enable mime detection on windows.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/2)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KSyntaxHighlighting - [Raku: fix fenced code blocks in Markdown](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/3)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 6 days.

- **Kate - [[gdbplugin] Port QRegExp to QRegular Expression](https://invent.kde.org/utilities/kate/-/merge_requests/83)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 7 days.

- **Kate - [[gdbplugin] Make extremely long variable names/values easier to view](https://invent.kde.org/utilities/kate/-/merge_requests/84)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 6 days.

#### Week 21

- **KTextEditor - [scrollbar minimap: performance: delay update for inactive documents](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/1)**<br />
Request authored by [Sven Brauch](https://invent.kde.org/brauch) and merged after one day.

- **KSyntaxHighlighting - [Add collaboration guide in README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/2)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Rename Perl6 to Raku and add new file extensions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/1)**<br />
Request authored by [Juan Francisco Cantero Hurtado](https://invent.kde.org/juanfra) and merged after one day.

#### Week 20

- **Kate - [Update debug target combobox when a target is selected](https://invent.kde.org/utilities/kate/-/merge_requests/82)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

#### Week 19

- **Kate - [Patch for Bug 408174. Bug when opening remote folder via SFTP.](https://invent.kde.org/utilities/kate/-/merge_requests/44)**<br />
Request authored by [Charles Vejnar](https://invent.kde.org/vejnar) and merged after 199 days.

- **Kate - [Highlighting Doc: expand explanation about XML files directory](https://invent.kde.org/utilities/kate/-/merge_requests/76)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 10 days.

- **Kate - [Fix starting LSP server on Windows](https://invent.kde.org/utilities/kate/-/merge_requests/79)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

