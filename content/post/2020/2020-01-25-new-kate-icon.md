---
title: New Kate Icon
author: Christoph Cullmann
date: 2020-01-25T20:57:00+02:00
url: /post/2020/2020-01-25-new-kate-icon/
---

For years, Kate had a very generic icon.
Unlike most other editors, that have very distinctive ones, we went with an icon that represented the use case of the program but provided no branding.

In 2014, we tried to improve our branding by introducing a mascot - [Kate the Woodpecker](/2014/10/12/kates-mascot-kate-the-woodpecker/).
Thought we used that in some places, like on the web site and in the about dialog, overall, the only thing most people did see was the generic icon (that even differs a lot between different icon themes).

I was not very happy with this and reached out last year to [Tyson Tan](https://tysontan.com/) to improve on this, given he already provided our mascot design.
I wanted to have some distinct icon that matches a bit the idea we had with the mascot.

After some iterations this process has lead to a new icon for our lovely text editor as can be seen below.

<p align="center">
    <a href="/images/kate-source-original.svg" target="_blank"><img width=512 src="/images/kate-source-original.svg"></a>
</p>

Tyson "gifted" this artwork to our project, this means we can license it how we see fit.

Therefore I consider it at the moment licensed like the most parts of kate.git or ktexteditor.git, as LGPLv2+ (or, alternatively, to be able to
share it without issues e.g. on Wikipedia and Co., as [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)).

But please, don't start using that icon for other applications ;=) Let's be friendly.

I started to spread the use of the new icon to the about dialogs of both Kate and KatePart and as application icon for Kate.
Beside this, I tried to update as many of our web sites as possible with the new icon.

If you spread the word about Kate, please consider to use the new icon.
If you contribute/host some web site that shows the old artwork, consider to update to the new one, we have a nicely scalable version [here](/images/kate-source-original.svg).

Thanks again to [Tyson Tan](https://tysontan.com/) for working on this in his spare time!

And thanks to all people contributing to and using Kate, without them, we wouldn't need any icon at all :-)
I hope you enjoy our new artwork.
