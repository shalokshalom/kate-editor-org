---
title: The Planet is on Reddit
author: Dominik Haumann
date: 2019-10-01T22:01:00+02:00
url: /post/2019/2019-10-01-the-planet-is-on-reddit/
---

For many years [planet.kde.org](https://planet.kde.org) was the goto page for news around KDE. This is still the case nowadays - many KDE contributors have their blog synchronized to talk about all sorts of KDE related cool stuff.

However, what changed significantly is *how* these blogs are discussed afterwards.

In the old days each blog typically had its own comment section. Nowadays, blogs may still have this comment section, but most of the time the blogs are discussed in the respective reddit subgroup. For instance, a popular subreddit is [reddit/r/kde](https://www.reddit.com/r/kde/). There, you can find many KDE developers as well as many KDE users, giving direct feedback, asking questions, sometimes mentioning bug reports. While KDE of course has a dedicated [bug tracker bugzilla](https://bugs.kde.org) that should be used, discussing a bug or wish on reddit often reaches a broader audience, putting some focus on specific bugs.

Similarly, there is also a subreddit for C++ called [reddit/r/cpp](https://www.reddit.com/r/cpp/) where many C++ experts (compiler developers, members of the C++ ISO committee) are around. Following this reddit certainly also makes sense for Qt developers. Then of course, there are subreddits like [reddit/r/linux](https://www.reddit.com/r/linux/) etc. where KDE also pops up from time to time.

So in conclusion, via these subreddits we are able to reach many more communitites than just the KDE community. So if you are a blogger on [planet.kde.org](https://planet.kde.org), make sure to post your blogs on the KDE subreddit. And in case reddit is new to you, you should definitely check it out from time to time!

For Kate, we decided to not have comments at all anymore. Instead, we simply always post our Kate blogs on the KDE subreddit for discussion, just like [for this post](https://www.reddit.com/r/kde/comments/dbyynm/the_planet_is_on_reddit/).
