---
title: KF6 Sprint - Day Three
author: Christoph Cullmann
date: 2019-11-24T12:29:00+02:00
url: /post/2019/2019-11-24-kf6-sprint-day-three/
---

Last day of the KF6 sprint at the [MBition](https://mbition.io/en/home/index.html) office in Berlin.

Yesterday evening we had a nice group dinner with all sprint members.

That is the starting state of our board today:

<p align="center">
    <a href="/post/2019/2019-11-24-kf6-sprint-day-three/images/workboard-morning.jpg" target="_blank"><img width=500 src="/post/2019/2019-11-24-kf6-sprint-day-three/images/workboard-morning.jpg"></a>
</p>

First we discussed about the larger things like kio, kparts and kxmlgui together in one large group (including remote David Faure).

This did lead to the following pre-lunch state of the board:

<p align="center">
    <a href="/post/2019/2019-11-24-kf6-sprint-day-three/images/workboard-lunch.jpg" target="_blank"><img width=500 src="/post/2019/2019-11-24-kf6-sprint-day-three/images/workboard-lunch.jpg"></a>
</p>

Afterwards again groups will look at individual frameworks on their own first, before some debrief session.

Unfortunately I will have too leave today already at afternoon to catch my train back to Saarbrücken.

Here some short overview over the room we met in at MBition.

<p align="center">
    <a href="/post/2019/2019-11-24-kf6-sprint-day-three/images/sprint-people-1.jpg" target="_blank"><img width=500 src="/post/2019/2019-11-24-kf6-sprint-day-three/images/sprint-people-1.jpg"></a>
</p>

<p align="center">
    <a href="/post/2019/2019-11-24-kf6-sprint-day-three/images/sprint-people-2.jpg" target="_blank"><img width=500 src="/post/2019/2019-11-24-kf6-sprint-day-three/images/sprint-people-2.jpg"></a>
</p>

Thanks again to both [MBition](https://mbition.io/en/home/index.html) and [KDE e.V.](https://ev.kde.org) to sponsor the sprint location and grant travel support.

Again, like yesterday, for all concrete results, we created tasks on the [KF6 board](https://phabricator.kde.org/tag/kf6/) on the KDE Phabricator.

This for sure needs to be further expanded, sorted and refined, but if you like to help out, this work board might be the best initial place to get some overview about what work is available and to give feedback.
