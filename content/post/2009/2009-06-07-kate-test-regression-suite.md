---
title: Kate Test Regression Suite
author: Dominik Haumann

date: 2009-06-07T11:55:00+00:00
url: /2009/06/07/kate-test-regression-suite/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2009/06/kate-test-regression-suite.html
categories:
  - Developers

---
This blog is mainly about documenting the process how to run Kate Part&#8217;s test regression suite and probably not much use for other developers.

Step I: Setup the environment

  1. create the file ~/.testkateregression. The content of this file is a single line pointing to the folder of the checkout of the test regression suite data. For me this is  
    /home/dh/kde/tests/katetests/regression/ 
  2. Create the folder where you checkout the suite  
    mkdir /home/dh/kde/tests
  3. Change to this folder
  4. Checkout the data:  
    svn co svn://anonsvn.kde.org/home/kde/trunk/tests/katetests

Now all the data is there for testing.

Step II: Run the test suite

  1. go to your build directory of kate (e.g. kdelibs/build/kate/tests/
  2. run the complete suite:  
    ./testkateregression.shell
  3. run specific tests, e.g. for the c indenter:  
    ./testkateregression.shell -t indent/csmart

That&#8217;s it.