---
title: 'KDevelop & KWrite/Kate hacksprint 2010 in Berlin'
author: Milian Wolff

date: 2009-11-16T15:38:01+00:00
excerpt: |
  Hey everybody!
  
  If you are a KDevelop and/or Kate/Kwrite developer and do not read the mailing lists: There&#8217;s a hack sprint coming up in Berlin in 2010. I think there&#8217;ve been enough sprints in Berlin already so that you know it&#8217;s a gr...
url: /2009/11/16/kdevelop-kwritekate-hacksprint-2010-in-berlin/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
"rss:comments":
  - 'http://milianw.de/blog/kdevelop-kwritekate-hacksprint-2010-in-berlin#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/108
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/kdevelop-kwritekate-hacksprint-2010-in-berlin
syndication_item_hash:
  - 13c25cae527660ec968043d6284fde3c
categories:
  - Events

---
Hey everybody!

If you are a KDevelop and/or Kate/Kwrite developer and do not read the mailing lists: There&#8217;s a hack sprint coming up in Berlin in 2010. I think there&#8217;ve been enough sprints in Berlin already so that you know it&#8217;s a great city for such an event. Though this time it won&#8217;t be at KDAB or Nokia offices, but at the Physics Faculty of the FU-Berlin. Since I (currently) work there as an IT admin, it was my first choice and worked out. I hope it will be a good location for the meeting. If you want to attend, vote on doodle:

[http://www.doodle.com/vkyh9up9794zr4s8][1]

But you probably also should register either on the KDevelop or KWrite mailing lists so I have some kind of way to contact you.

PS: in unrelated news I&#8217;ll do an internship at KDAB next year! yay

 [1]: http://www.doodle.com/vkyh9up9794zr4s8 "http://www.doodle.com/vkyh9up9794zr4s8"