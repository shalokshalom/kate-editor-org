---
title: Support Kate and KDE by Donating Money
author: Dominik Haumann

date: 2014-10-18T13:46:59+00:00
url: /2014/10/18/support-kate-and-kde-by-donating-money/
categories:
  - Users
tags:
  - planet

---
Since some weeks, the Kate homepage features a way to support Kate and KDE by donating money to the KDE e.V., see this screenshot:

<img class="aligncenter size-full wp-image-3441" src="/wp-content/uploads/2014/10/kate-donate.png" alt="Donating Money to Kate" width="673" height="475" srcset="/wp-content/uploads/2014/10/kate-donate.png 673w, /wp-content/uploads/2014/10/kate-donate-300x211.png 300w" sizes="(max-width: 673px) 100vw, 673px" /> 

The reason for showing a donation pane might not be obvious, since the KDE project is open source and mostly developed by lots of contributors in their free time. So why do we need money?

The KDE project, and thus also the Kate project, highly depends on getting financial support. For instance, our IT infrastructure running the KDE websites and all sorts of services such as mailing lists or hosting KDE&#8217;s source code along with the version control systems rely on it. All these services need money. And here is where the KDE e.V., KDE&#8217;s non-profit organization comes into play. KDE e.V.&#8217;s <a title="KDE e.V. Mission Statement" href="https://ev.kde.org/whatiskdeev.php" target="_blank">purpose</a> is _the promotion and distribution of free desktop software in terms of free software, and KDE in particular, to promote the free exchange of knowledge and equality of opportunity in accessing software as well as education, science and research_.

For instance, the KDE e.V. supports developers through travel reimbursement, such that contributors that could otherwise not attend <a title="Developer Meetings" href="https://ev.kde.org/activities/devmeetings/" target="_blank">developer meetings</a> are still able to take part. These developer meetings have proven to be immensely useful to the success of KDE, and typically a developer sprint moves a project forward by magnitues, as you can see in the <a title="List of Developer Strints" href="https://community.kde.org/KDE_e.V./Sprints" target="_blank">list of past developer meetings</a>. Next to that, there is also the <a title="Annual KDE Conference" href="https://akademy.kde.org/" target="_blank">annual KDE conference</a> where all KDE contributors and users are invited to discuss and shape the future of KDE. Next to other events, KDE is usually also present at fairs such as the CeBit or LinuxTag. There, we also need material and support to make a good presentation of KDE. Another significant job of KDE e.V. is to support KDE legally. For instance, the KDE e.V. is maintaining an agreement with the owners of Qt in terms of the <a title="KDE Free Qt Foundation" href="https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php" target="_blank">KDE Free Qt Foundation</a>, which potentially is also of high interest for companies using Qt.

Several days ago we also started the <a title="KDE End of Year 2014 Fundraiser" href="https://www.kde.org/fundraisers/yearend2014/" target="_blank">KDE End of Year 2014 Fundraiser</a>, through which we hope to get a significant amount of money that we can plan with in the next year.

Please, if you use KDE at home or in your company, please <a title="Donate to KDE" href="https://www.kde.org/fundraisers/yearend2014/" target="_blank">make a donation</a>. And if you are a company, please consider being generous! Your support is much more needed and appreciated than you might think! Thanks you!