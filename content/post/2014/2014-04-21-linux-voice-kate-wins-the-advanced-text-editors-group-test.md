---
title: Linux Voice – Kate wins the advanced text editors group test
author: Christoph Cullmann

date: 2014-04-21T16:21:23+00:00
url: /2014/04/21/linux-voice-kate-wins-the-advanced-text-editors-group-test/
categories:
  - Common
  - KDE
  - Users
tags:
  - planet

---
Happy news arrived on the kwrite-devel mailing list <a href="http://lists.kde.org/?l=kwrite-devel&#038;m=139776572524753&#038;w=2" title="Linux Voice Verdict" target="_blank">with this post</a> before Easter ;)

Kate has won the advanced text editor comparison in the <a title="Linux Voice" href="http://www.linuxvoice.com/" target="_blank">Linux Voice</a> magazine &#8211; <a title="Linux Voice - Issue 2" href="http://www.linuxvoice.com/issue-2-is-out/" target="_blank">Issue 2</a>, yeah ;)

Nice to see that our work on Kate is awarded.

About Linux Voice: Seems to be some pretty new magazine about Linux & Open Source (Issue 2 says it all) and they promise to give 50% of their profit back to the Free Software community, developers and events. Hope that works out, it is a nice goal.

P.S. And yeah, this is just one comparison and no, lets not start the &#8220;editors wars, &#8230; edition&#8221; in the comments, there are a lot of text editors to choose between and depending on your needs and preferences Kate might not be your favorite ;)

[<img class="wp-image-3338 alignright" title="Linux Voice" alt="Featured  in Linux Voice" src="/wp-content/uploads/2014/04/lv_button.png" width="300" height="123" srcset="/wp-content/uploads/2014/04/lv_button.png 500w, /wp-content/uploads/2014/04/lv_button-300x123.png 300w" sizes="(max-width: 300px) 100vw, 300px" />][1]

 [1]: http://www.linuxvoice.com/