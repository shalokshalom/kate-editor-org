---
title: Treats for Kate - Welcome Page, Git Diff Viewer, Config Searching
author: Eric Armbruster
date: 2022-10-31T18:35:00+02:00
url: /post/2022/2022-10-31-treats-for-kate/
---

It's Halloween and the Kate and KWrite 22.12 release is approaching, so its about time we give you another update what we've been working on.

## The Biggest Treat: Widgets

So far Kate was only able to display code views in Kate's central view component. In the upcoming release, Kate can also place arbitrary Qt Widgets there, which required making some bigger changes under the hood.
You might ask how is this relevant at all, to which we answer: this change enabled us to implement a treat bag of cool, new features. Already we have used this to implement a welcome page, an improved git diff viewer and the configuration options now reside in a tab instead of a dialog.

### Welcome Page with Recent Docs, Projects and Sessions

As some of you might have already heard from Nate's This Week in KDE posts, Kate and KWrite got a welcome page just like many other KDE apps already have one. It offers among other things a list of recent sessions, documents and **projects** (recent projects are of course now also available from the Open Recent menu).

![Kate 22.12 welcome page](/post/2022/2022-10-31-treats-for-kate/images/kate-welcome-page.png)
![KWrite 22.12 welcome page](/post/2022/2022-10-31-treats-for-kate/images/kwrite-welcome-page.png)

We think the welcome page is a good starting point for newbies as well as more advanced users, but we've also heard the feedback from some that want to immediately start typing or pasting something when they open Kate or KWrite and the welcome page would annoyingly slow them down in their workflow. To provide the best experience for everybody, we've added a checkbox (labeled "Show for new window") on the welcome page that, if unchecked, will cause the page to stop displaying.

### Git Diff Viewer

Diff viewing in Kate just got a huge update with the new diff viewer widget that is able to highlight diffs at word level. The new diff viewer also lets you jump between diffs, switch between different viewing styles (unified, side-by-side and raw) and you can use it to (un)stage individual hunks and lines.

<video width=100% controls>
<source src="/post/2022/2022-10-31-treats-for-kate/videos/diff-viewer.mp4" type="video/mp4">
Your browser does not support the video tag :P</video>

### Configuration Tab with Search Functionality and Highlighting

Kate and KWrite just got a configuration tab, i.e. gone are the days of the old modal configuration dialog, which did not allow you to quickly apply a setting, see its effect and then revert it back. Additionally, you can now search for configuration options by their name and the application will automatically highlight matching options for you.

<video width=100% controls>
<source src="/post/2022/2022-10-31-treats-for-kate/videos/config-search.mp4" type="video/mp4">
Your browser does not support the video tag :P</video>

## New Clipboard History Paste Dialog

All KTextEditor based editors offer a new clipboard history paste dialog since Frameworks 5.98. By default it can be invoked through Ctrl+Shift+Alt+V or from the Edit menu. It shows a code preview of the currently selected clipboard entry and easily lets you select the clipboard entry you want to paste.

<video width=100% controls>
<source src="/post/2022/2022-10-31-treats-for-kate/videos/clipboard-history-dialog.mp4" type="video/mp4">
Your browser does not support the video tag :P</video>

## More Treats?

Of course there is still much more stuff in this release. Some of this is listed below, but naturally not everything can be covered in a single blogpost:

- Various improvements to the build plugin
- More actions that are commonly used now have a default shortcut assigned
- Sidebar buttons can now be moved into an own section by right-clicking on them. This allows opening multiple toolviews in one sidebar simultaneously
- Documents can now be detached into a new window from the tab context menu
- The status bar uses less space and is now configurable

## Get Involved

We encourage you to have a look at our [merge requests](/merge-requests/), see what else is going on there and maybe start working towards your very first own contribution. We promise, we don't bite ;)

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/yiiv71/treats_for_kate_welcome_page_git_diff_viewer/).
