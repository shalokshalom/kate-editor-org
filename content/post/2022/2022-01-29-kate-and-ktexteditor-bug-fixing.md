---
title: Kate & KTextEditor Bug Fixing
author: Christoph Cullmann
date: 2022-01-29T22:40:00+02:00
url: /post/2022/2022-01-29-kate-and-ktexteditor-bug-fixing/
---

## Bugs, bugs, bugs...

The Kate & KTextEditor projects have at the moment [189 open bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&list_id=1970098&product=frameworks-ktexteditor&product=kate).

That is a lot given only a few people work on this projects regularly.

Therefore it would be really appreciated if people could help out with fixing.

If you have spare time and you are interested in help to improve our projects, head over to our [long list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&list_id=1970098&product=frameworks-ktexteditor&product=kate).

Some might even be already fixed and just need to be tried out with a recent version.
Other might lack ways to reproduce, providing such ways or even just asking the reporter and setting the bug to the NEEDSINFO/WAITINGFORINFO state will help us to keep some better overview.

Contributing to Kate & KTextEditor is easier then ever nowadays.

Head over to our [build documentation](/build-it/), with kdesrc-build it is very easy to setup some development environment on any recent Linux/BSD distribution.

We have already a really large number of [merge requests](/merge-requests/) that can be used as hints how to actually submit you patches and how the process works.
Normally, if you are responsive to our feedback, I would say the patch contribution works really well since we are on our GitLab instance.

## Wishes?

Beside actual bugs, we have [128 wish list items](https://bugs.kde.org/buglist.cgi?bug_severity=wishlist&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&list_id=1970099&product=frameworks-ktexteditor&product=kate) lingering around.

If you need some ideas what could be implemented, you can take a look at [that list](https://bugs.kde.org/buglist.cgi?bug_severity=wishlist&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&list_id=1970099&product=frameworks-ktexteditor&product=kate), too.

We have some more high-level collection of ideas to work on our [GitLab instance](https://invent.kde.org/utilities/kate/-/issues/20).

Naturally, you might be more interested in contributing patches for your own ideas, that is definitively welcome!

These lists are just collection of potential things you could work on if you have no own ideas but want to help out with feature development for people that can't implement them on their own.

## Rambling of a maintainer about bug reporting

Now, after reaching out for a bit help with the bugs and wishes we already have around, some feedback for the actual bug reporters.

I naturally appreciate that you report bugs and wishes.

Only with such feedback we are able to know what is broken and what is wanted.

This is very important.

Sometimes even I myself just report a bug for some crash or other misbehavior that I can't directly fix or reproduce, just to have it marked for future review.

And it is totally valid to report wishes you have for our stuff.

But, there is the other side, too, our team.

We work on this in our spare time, no Kate or KTextEditor main contributor was ever paid for that.
I guess more or less all current work is done purely as hobby and out of fun or responsibility for the code base.

Implementing new features is fun, normally you add stuff you wants to use yourself, therefore the work on that is gratifying.

On the other side, fixing bugs is often tedious work.
Even more, if the bug doesn't hurt you at all.

Just to pick a random example (we had a input method bug like this, but without any of the negative feedback mentioned at the end, that's fiction for that bug):

Some input method breaks.
I don't use any complex input method, therefore I am not hurt by that at all.
As long as I can type äüöß... I am happy.
But yes, naturally, such a breakage is very bad for a large part of the user base.

I personally have a hard time to fix such stuff myself, as I have close to zero experience with it and have not even implemented the current support for that.
In such cases if I find the time I try to reach out for people that are more fluent with that and eventually such things then get worked out.

If nobody is found that actually cares for such things or no such person has time, such bugs will stay.
No amount of pinging, shouting or similar inside the bug will help.
I feel responsible as maintainer, but if I have not the needed skill or time or both nothing will happen.
I can not coerce somebody to fix it even if I know somebody that has the needed skills.
Nor do I want to.

Over the last years, we had such negative events in not that few bugs.
Sometimes it seems like people think we are actually their paid support.
No, we are not.
If you report some issue and we ignore it, ping us friendly, that can't hurt.
For sure at least I did already just ignore bugs because I didn't read them well enough or just skipped them completely.
Some gentle ping is ok, but some "this is very important and must be directly fixed or else I will never ever use this again" won't help you.

The same if you want some behavior change or feature and we don't want to implement this.
Let's say somebody wishes in a bug "I want the option that the text is painted in rainbow colors with some configurable rainbow color scheme".
And then I and others in the team decide this is a feature out of scope for us and we won't do it.
You should respect this.
Even more if you actually don't want to contribute such a behavior change or feature yourself.

## Executive summary about bug reporting

Bugs & wishes are very welcome, you help us to improve our projects by reporting them.

But keep in mind, we have not sold you some product with a support contract that will earn us our living.

We provide you with hopefully valuable tools in our spare time and we would appreciate at least a bit respect for that in the communication with us.

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/sft207/kate_ktexteditor_bug_fixing/).
