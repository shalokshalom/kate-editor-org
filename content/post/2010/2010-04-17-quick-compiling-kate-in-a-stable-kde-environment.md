---
title: Quick Compiling Kate in a stable KDE Environment
author: Dominik Haumann

date: 2010-04-17T12:10:00+00:00
url: /2010/04/17/quick-compiling-kate-in-a-stable-kde-environment/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/04/quick-compiling-kate-in-stable-kde.html
categories:
  - Developers
  - Users

---
Since all of the [Kate code is now co-hosted on gitorious][1], it became very easy to build Kate in your stable KDE >= 4.4 environment. This means you can run the newest version of Kate with very few effort. Just give it a try and do the following steps:

  1. make sure you have the following packages installed: git, cmake and kdelibs development package (on openSUSE this is <span style="font-family: courier new;">git</span>, <span style="font-family: courier new;">cmake</span> and <span style="font-family: courier new;">libkde4-devel</span>)
  2. create and change into a KDE development directory:  
    <span style="font-family: courier new;">mkdir ~/kde; cd ~/kde</span>
  3. get a copy of the Kate code:  
    <span style="font-family: courier new;">git clone git://gitorious.org/kate/kate.git</span>
  4. create and change into a build directory for compilation:  
    <span style="font-family: courier new;">mkdir build; cd build</span>
  5. run the configure process with cmake:  
    <span style="font-family: courier new;">cmake ../kate -DCMAKE_BUILD_TYPE=debugfull \<br /> -DCMAKE_INSTALL_PREFIX=~/kde/usr</span>
  6. compile Kate:  
    <span style="font-family: courier new;">make</span>
  7. finally install Kate:  
    <span style="font-family: courier new;">make install</span>

That&#8217;s all! This installs Kate locally into the separate directory <span style="font-family: courier new;">~/kde/usr</span>, so that your global KDE installation will not be touched at all.

Now on to starting the newly compiled Kate. Create a file ~/kde/run.sh with the following content:

<pre>#!/bin/bashexport KATE_DIR=~/kde/usr
export PATH=$KATE_DIR/bin:$PATH
export LD_LIBRARY_PATH=$KATE_DIR/lib:$LD_LIBRARY_PATH
export KDEDIR=$KATE_DIR
export KDEDIRS=$KDEDIRexport XDG_DATA_DIRS=$XDG_DATA_DIRS:$KATE_DIR/share
# update KDE's system configuration cache
kbuildsycoca4
# start app
$@</pre>

Now you can run the compiled Kate version with <span style="font-weight: bold; font-family: courier new;">~/kde/run.sh kate</span>. Just calling <span style="font-weight: bold; font-family: courier new;">kate</span> directly will start the system version of Kate.

Your copy of Kate contains [all of the Kate code][2], i.e.: the KTextEditor interfaces, Kate Part, KWrite and the Kate Application. Feel free to send patches to our mailing list <kwrite-devel@kde.org>. And join [#kate on irc.libera.chat][3] :-)

Note for KDE developers: All the changes in git are merged back to KDE&#8217;s subversion repository in a timely manner. So don&#8217;t worry about Kate moving away from KDE; this is not the case.

<span style="font-style: italic;">(Updated on 2010-04-17: Allow installation into local directory.</span>)

 [1]: http://gitorious.org/kate
 [2]: http://gitorious.org/kate/kate/trees/master
 [3]: irc://irc.libera.chat/kate
