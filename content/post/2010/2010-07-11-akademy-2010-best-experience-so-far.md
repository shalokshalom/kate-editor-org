---
title: Akademy 2010 – Best experience so far
author: dianat

date: 2010-07-11T18:23:13+00:00
url: /2010/07/11/akademy-2010-best-experience-so-far/
categories:
  - Events
tags:
  - planet

---
[<img class="aligncenter size-medium wp-image-388" src="/wp-content/uploads/2010/07/logo_akademy-sq-300x300.png" alt="" width="300" height="300" srcset="/wp-content/uploads/2010/07/logo_akademy-sq-300x300.png 300w, /wp-content/uploads/2010/07/logo_akademy-sq-150x150.png 150w, /wp-content/uploads/2010/07/logo_akademy-sq.png 500w" sizes="(max-width: 300px) 100vw, 300px" />][1]

My first Akademy was the greatest thing I&#8217;ve ever experienced. I remember I was quite unsure a month ago, wheter to come or not, but finally decided and booked my flight and hotel. In my first day, I was very shy and didn&#8217;t know what to do, but people came and talked to me and everything got better. The talks had very interesting topics and the speakers did their job professionally. I also want to congratulate the organization team, for making everything happen as scheduled.

The BoFs at Demola were even more interesting. You go there, find a place to sit, and start hacking with many other KDE developers. I never did this before and it is simply amazing. You can&#8217;t believe the things that can come out from these hacking days/nights :).

I have never thought that KDE is such a great community, but now I am convinced. Akademy gave me a lot of confidence ( especially &#8220;Highlights on KDE Women&#8221; talk :P ) and now I really feel I can bring something to KDE. I am a little sad because I couldn&#8217;t attend the whole Akademy, but I think that more Akademies are to come :).

 [1]: /wp-content/uploads/2010/07/logo_akademy-sq.png