---
title: Kate Night-Make
author: Christoph Cullmann

date: 2010-08-23T19:31:08+00:00
url: /2010/08/23/kate-night-make/
categories:
  - Developers
tags:
  - planet

---
Each night now Kate (part/app) is build from git and all unit tests are run. Yeah, we even got some tests .P  
Until now, that just reminds on of our failures ;) But I hope the daily mail will perhaps motivate me and others more to fix them ;)

Beside, thanks for all the people working to write these tests, like Bernhard, Dominik and Milian.

<pre>Kate Nightmake Tests
From: cullmann@kate-editor.org
To: kwrite-devel@kde.org
Date: Today 05:06:46

[HANDLER_OUTPUT]
Test project /home/www/kate-editor.org/build/build

Start  1: katetextbuffertest
1/34 Test  #1: katetextbuffertest ...............   Passed    0.02 sec
Start  2: range_test
2/34 Test  #2: range_test .......................   Passed    1.09 sec
Start  3: testkateregression
3/34 Test  #3: testkateregression ...............***Failed    0.03 sec
Start  4: undomanager_test
4/34 Test  #4: undomanager_test .................   Passed    1.34 sec
Start  5: plaintextsearch_test
5/34 Test  #5: plaintextsearch_test .............   Passed    7.56 sec
Start  6: regexpsearch_test
6/34 Test  #6: regexpsearch_test ................   Passed    8.45 sec
Start  7: scriptdocument_test
7/34 Test  #7: scriptdocument_test ..............   Passed    1.36 sec
Start  8: completion_test
8/34 Test  #8: completion_test ..................***Failed    0.95 sec
Start  9: searchbar_test
9/34 Test  #9: searchbar_test ...................***Failed   11.24 sec
Start 10: movingcursor_test
10/34 Test #10: movingcursor_test ................   Passed    0.94 sec
Start 11: movingrange_test
11/34 Test #11: movingrange_test .................   Passed    1.79 sec
Start 12: katedocument_test
12/34 Test #12: katedocument_test ................   Passed    1.09 sec
Start 13: revision_test
13/34 Test #13: revision_test ....................   Passed    0.63 sec
Start 14: templatehandler_test
14/34 Test #14: templatehandler_test .............   Passed    0.44 sec
Start 15: indenttest
15/34 Test #15: indenttest .......................***Failed   25.87 sec
Start 16: bug213964_test
16/34 Test #16: bug213964_test ...................   Passed    0.73 sec
Start 17: utf8.txt_create
17/34 Test #17: utf8.txt_create ..................   Passed    0.17 sec
Start 18: utf8.txt_diff
18/34 Test #18: utf8.txt_diff ....................   Passed    0.01 sec
Start 19: latin15.txt_create
19/34 Test #19: latin15.txt_create ...............***Exception: SegFault  0.04 sec
Start 20: latin15.txt_diff
20/34 Test #20: latin15.txt_diff .................***Failed    0.01 sec
Start 21: utf32.txt_create
21/34 Test #21: utf32.txt_create .................   Passed    0.13 sec
Start 22: utf32.txt_diff
22/34 Test #22: utf32.txt_diff ...................   Passed    0.01 sec
Start 23: utf16.txt_create
23/34 Test #23: utf16.txt_create .................   Passed    0.10 sec
Start 24: utf16.txt_diff
24/34 Test #24: utf16.txt_diff ...................   Passed    0.01 sec
Start 25: utf32be.txt_create
25/34 Test #25: utf32be.txt_create ...............   Passed    0.10 sec
Start 26: utf32be.txt_diff
26/34 Test #26: utf32be.txt_diff .................   Passed    0.01 sec
Start 27: utf16be.txt_create
27/34 Test #27: utf16be.txt_create ...............   Passed    0.10 sec
Start 28: utf16be.txt_diff
28/34 Test #28: utf16be.txt_diff .................   Passed    0.01 sec
Start 29: cyrillic_utf8.txt_create
29/34 Test #29: cyrillic_utf8.txt_create .........   Passed    0.10 sec
Start 30: cyrillic_utf8.txt_diff
30/34 Test #30: cyrillic_utf8.txt_diff ...........   Passed    0.01 sec
Start 31: cp1251.txt_create
31/34 Test #31: cp1251.txt_create ................   Passed    0.10 sec
Start 32: cp1251.txt_diff
32/34 Test #32: cp1251.txt_diff ..................   Passed    0.01 sec
Start 33: koi8-r.txt_create
33/34 Test #33: koi8-r.txt_create ................   Passed    0.11 sec
Start 34: koi8-r.txt_diff
34/34 Test #34: koi8-r.txt_diff ..................   Passed    0.01 sec

82% tests passed, 6 tests failed out of 34

Total Test time (real) =  64.64 sec

The following tests FAILED:
3 - testkateregression (Failed)
8 - completion_test (Failed)
9 - searchbar_test (Failed)
15 - indenttest (Failed)
19 - latin15.txt_create (SEGFAULT)
20 - latin15.txt_diff (Failed)
[ERROR_MESSAGE]
Errors while running CTest
</pre>