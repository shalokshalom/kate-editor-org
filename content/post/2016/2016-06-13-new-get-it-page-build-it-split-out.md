---
title: 'New “Get It” page & “Build It” split out'
author: Christoph Cullmann

date: 2016-06-13T16:32:13+00:00
url: /2016/06/13/new-get-it-page-build-it-split-out/
categories:
  - Common
  - KDE
  - Users
tags:
  - planet

---
As we now have some binaries available for Windows & Mac, I renovated the &#8220;[Get It][1]&#8221; page.

You now have there links to the latest versions of our binaries (installer for Windows & application bundle for Mac) beside the link to the distribution packages for Linux/BSD/* as provided by our dear packagers.

For building Kate from sources, we now have the building instructions on a separate &#8220;[Build It][2]&#8221; page.

We will try to update the binary builds in the next days at [Randa][3].

[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][4]

 [1]: /get-it/
 [2]: /build-it/
 [3]: https://community.kde.org/Sprints/Randa/2016
 [4]: https://www.kde.org/fundraisers/randameetings2016/