---
title: The (GSoC) season is ending…
author: Adrian Lungu

date: 2011-08-15T15:54:56+00:00
url: /2011/08/15/the-gsoc-season-is-ending…/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Hi everyone!

&nbsp;  
So… this GSoC season came to its end. This week is the official deadline for the projects and, happily, my project is done, but my collaboration with Kate is just starting, so I’ll see you around :)  
&nbsp;  
As I mentioned in the previous post, I used this last week for some fine tuning and to close the bugs that slipped the previous scan. The folding-related bugs remained on [bug.kde.org][1] are highlight related too, so I can’t do much about them. The folding submenu was changed, so we can add more features into the feature. You can find the old and the new folding’s submenus in this article. You can see that the new folding has new features and it’s more flexible. Now you can fold/unfold a specific level entirely, or you can fold/unfold the node that contains the cursor. I have one regret regarding these new features: we couldn’t find some key-shortcuts for them :(

[<img class="aligncenter size-large wp-image-1295" src="/wp-content/uploads/2011/08/snapshot1-1024x638.png" alt="" width="770" height="479" srcset="/wp-content/uploads/2011/08/snapshot1-1024x638.png 1024w, /wp-content/uploads/2011/08/snapshot1-300x187.png 300w, /wp-content/uploads/2011/08/snapshot1.png 1049w" sizes="(max-width: 770px) 100vw, 770px" />][2]

&nbsp;  
The last feature added is: collapse all multi-line comments. This was a wish from bugs.kde.org. I couldn’t solve it entirely (to collapse all comments at once) because, as you might know from my previous articles, Kate’s folding is based on Kate’s highlight; it doesn’t have its own parser. There is no highlight for single-line comments, so we can’t fold them. But I hope this will help too (it might prove very useful in large XML files for example).  
&nbsp;  
Another cool (new feature) is remembering the folded lines. I don’t remember having talked about it so far. :)  
What does that mean? Well, if you work hard to fold a lot of lines and you accidentally close your file, you don’t have to take it over again. All the folded nodes remain folded when you close/open a file or when you reload it. Pretty nice, don’t you think ? ;)  
&nbsp;  
The new folding will probably be included in version 4.8. For now, you can use it if you [download Kate’s sources and compile them][3].  
Finally, I would like to thank you guys for supporting me and my project and if you didn’t use Kate’s new code folding, then get the last sources, compile them and let the magic begin! :P  
&nbsp;  
Greetings,  
Adrian

 [1]: https://bugs.kde.org/
 [2]: /wp-content/uploads/2011/08/snapshot1.png
 [3]: /get-it/