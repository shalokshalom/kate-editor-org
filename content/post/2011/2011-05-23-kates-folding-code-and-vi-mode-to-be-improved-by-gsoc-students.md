---
title: Kate’s Folding Code and Vi Mode to be improved by GSoC students
author: Erlend Hamberg

date: 2011-05-23T20:30:34+00:00
url: /2011/05/23/kates-folding-code-and-vi-mode-to-be-improved-by-gsoc-students/
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet

---
This year&#8217;s Google Summer of Code (GSoC) has started and the Kate project has been lucky to get two students who will work on improving Kate&#8217;s folding code and Vi input mode.

<figure id="attachment_895" aria-describedby="caption-attachment-895" style="width: 192px" class="wp-caption alignright">[<img class="size-medium wp-image-895  " title="adrian" src="/wp-content/uploads/2011/05/adrian-300x225.jpg" alt="" width="192" height="144" srcset="/wp-content/uploads/2011/05/adrian-300x225.jpg 300w, /wp-content/uploads/2011/05/adrian-1024x768.jpg 1024w, /wp-content/uploads/2011/05/adrian.jpg 1600w" sizes="(max-width: 192px) 100vw, 192px" />][1]<figcaption id="caption-attachment-895" class="wp-caption-text">Adrian</figcaption></figure>

Adrian will work on improving the folding code which is in need of an overhaul:

<p style="padding-left: 30px;">
  My name is Adrian. I’m studying in Bucharest, Romania, but my hometown is Constanta, a seaside town from Romania as well. I am a 3rd year student at “Politehnica” University of Bucharest, majoring in Computer Science and Engineering. I have developed a passion for algorithms since High school when I participate in many programming competitions and took things to a new level during college.
</p>

<p style="padding-left: 30px;">
  My GSoC project is called “Kate Code Folding” and I believe its name is quite explicit.  I chose this project because I am very familiar with the editor, as well as its code folding bugs. :)  Besides that, I am pretty excited that I have to develop a new algorithm for this project and I must say I find this task very challenging. For a better understanding of my project, here there are 2 paragraphs from my proposal, with no details, just the main ideas:
</p>

<p style="padding-left: 30px;">
  My project idea is based on two elements. The first one is a new approach of the problem: transform one more complex problem into two simpler problems. To be more specific, as far as I know there are two types of programming languages: there are languages that use syntactical elements like {} or any other begin/end constructions (e.g.: C/C++) and there are languages that use indentation level to define their code blocks (e.g.: Python).
</p>

<p style="padding-left: 30px;">
  The second idea is to have the new implementations compatible with the current one. What I mean is that most of the actual public functions will still be used in the new implementation (some of them will suffer a few modifications), to solve the dependencies problem in a smart and simple way. So there won’t be too many changes in the other source files.
</p>

<figure id="attachment_896" aria-describedby="caption-attachment-896" style="width: 155px" class="wp-caption alignright">[<img class="size-medium wp-image-896 " title="svyatoslav" src="/wp-content/uploads/2011/05/svyatoslav-243x300.jpg" alt="" width="155" height="192" srcset="/wp-content/uploads/2011/05/svyatoslav-243x300.jpg 243w, /wp-content/uploads/2011/05/svyatoslav-831x1024.jpg 831w, /wp-content/uploads/2011/05/svyatoslav.jpg 952w" sizes="(max-width: 155px) 100vw, 155px" />][2]<figcaption id="caption-attachment-896" class="wp-caption-text">Svyatoslav</figcaption></figure>

Svyatoslav will work on improving the Vi input mode in Kate:

<p style="padding-left: 30px;">
  I am Svyatoslav Kuzmich. I&#8217;m a 2nd year student of Moscow Institute of Physics and Technology, department of Radioengineering and Computer Science.I usually write something like emulators and compilers but this Summer I am doing GSoC for KDE. I want to improve Vi input mode for Kate kpart. There are a lot of features already implemented. In some ways they works rather good. But there are some commands do not work like commands in Vim. So I want to fix them and to expand the list of commands by adding some insert and command mode&#8217;s commands for working with Kate&#8217;s tabs, window splits and bookmarks.
</p>

In more behind-the-scenes work, Svyatoslav will also write an extensive test suite for the Kate Vi Mode to make it easier to introduce new features without at the same time introducing regressions. This work has already begun.

We will try to keep you updated throughout the summer. Please wish our GSoC students good luck! :-)

 [1]: /wp-content/uploads/2011/05/adrian.jpg
 [2]: /wp-content/uploads/2011/05/svyatoslav.jpg