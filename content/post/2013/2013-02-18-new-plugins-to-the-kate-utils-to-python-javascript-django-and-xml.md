---
title: 'New plugins to the Kate, utils to: Python, JavaScript, Django and XML'
author: Pablo Martin

date: 2013-02-18T09:17:00+00:00
url: /2013/02/18/new-plugins-to-the-kate-utils-to-python-javascript-django-and-xml/
pw_single_layout:
  - "1"
categories:
  - Developers
  - KDE
  - Users
tags:
  - django
  - planet
  - python

---
The project plugin in kate.git master now has four new more plugins, with many features in each one: <a title="Python" href="http://www.python.org/" target="_blank">Python</a> (autocomplete, smart snippets, parse checker, <a href="http://pypi.python.org/pypi/pep8" target="_blank">pep8</a> checker and <a href="http://pypi.python.org/pypi/pyflakes" target="_blank">pyflakes</a> checker), Javascript (autocompletes, jquery snippet, pretty JSON, and <a href="http://www.jslint.com/" target="_blank">jslint</a> checker), <a title="Django project" href="https://www.djangoproject.com/" target="_blank">Django</a> (smart snippets and utils to Django template) and XML (pretty xml). There are also many generic functions and generic classes in these that will be useful to the new plugins developers.

<!--more-->

I began to develop these plugins in 2009, this was [my first version][1] :-). I wanted to set a shortcut to a snippet, as it was not possible (now it is posible) I started to develop these plugins to Kate.

In November 2011, I created a repository on [github][2] to develop new features and share them. I have to thank <a href="https://github.com/ablanco" target="_blank">Alejandro Blanco</a>, <a href="https://github.com/phrearch" target="_blank">Jeroen van Veen</a>, <a href="https://github.com/javiromero" target="_blank">Javier Romero</a>, <a href="https://github.com/mborho" target="_blank">Martin Borho</a>, <a href="https://github.com/justinzane" target="_blank">Justin Chudgar</a> and <a href="http://www.yaco.es/" target="_blank">Yaco Sistemas</a> for helping me in this <a href="https://github.com/goinnn/Kate-plugins" target="_blank">project</a>.

In October 2012 Christoph Cullmann (Kate maintainer) sent me an e-mail, in which he asked me if I was interested to contribute with KDE project. I answered: &#8220;Of course!&#8221;&#8230;. finally I have a little time, and I have been able to integrate these plugins in the <a href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/" target="_blank">kate.git</a>

## Features to Python Plugin:

  * Python dynamic autocomplete using <a href="https://github.com/goinnn/pyplete" target="_blank">pyplete</a>, this is in beta&#8230; It requires development, but now it is very useful.
  * Smart snippets: 
      1. Insert an \_\_init\_\_ method into a class.
      2. Insert a super call into a method.
      3. Insert a call recursive into a method or into a function
  * Simple snippet: Insert the debug instrunctions &#8220;import ipdb; ipdb.set_trace()&#8221;
  * Checkers: When you save the file it is syntax checked, <a href="http://pypi.python.org/pypi/pep8" target="_blank">pep8</a> checked and <a href="http://pypi.python.org/pypi/pyflakes" target="_blank">pyflakes</a> checked. You can invocate them any other time.

## [  
][3] [<img class="aligncenter size-full wp-image-2250" src="/wp-content/uploads/2013/02/autocompletion_python3.jpg" alt="" width="1043" height="282" srcset="/wp-content/uploads/2013/02/autocompletion_python3.jpg 1043w, /wp-content/uploads/2013/02/autocompletion_python3-300x81.jpg 300w, /wp-content/uploads/2013/02/autocompletion_python3-1024x276.jpg 1024w" sizes="(max-width: 1043px) 100vw, 1043px" />][4]

<p style="text-align: center">
  Python auto complete
</p>

## Features to Javascript Plugin:

  * JavaScript static autocomplete, using a JSON file.
  * <a href="http://jquery.com/" target="_blank">JQuery</a> static autocomplete using a JSON file.
  * Pretty JSON: Pretty format of a JSON code selected
  * <a href="http://www.jslint.com/" target="_blank">JSLint</a> checker: When you save the file it is JSLint checked. You can invocate it any other time.
  * <a href="http://jquery.com/" target="_blank">JQuery</a> snippet, insert the ready code of the <a href="http://jquery.com/" target="_blank">jQuery</a>

## [<img class="aligncenter size-full wp-image-2257" src="/wp-content/uploads/2013/02/jslint1.jpg" alt="" width="763" height="324" srcset="/wp-content/uploads/2013/02/jslint1.jpg 763w, /wp-content/uploads/2013/02/jslint1-300x127.jpg 300w" sizes="(max-width: 763px) 100vw, 763px" />][5]

<p style="text-align: center">
  JSLint checker
</p>

## Features to Django Plugin:

  * Smart snippets: 
      1. Create a model class
      2. Create a form class
      3. Template to the urls.py file
  * Simple snippet: Insert the typical imports of a views.py file
  * Template Django utils 
      1. Insert the tag block/endblock. The name of the block will be the text selected
      2. Close the last templatetag open

## 

## [<img class="aligncenter size-full wp-image-2262" src="/wp-content/uploads/2013/02/django_form1.jpg" alt="" width="368" height="321" srcset="/wp-content/uploads/2013/02/django_form1.jpg 368w, /wp-content/uploads/2013/02/django_form1-300x261.jpg 300w" sizes="(max-width: 368px) 100vw, 368px" />][6]

<p style="text-align: center">
  Create a form class
</p>

## Pretty XML:

Pretty format of an XML code selected

## 

## [<img class="aligncenter size-full wp-image-2264" src="/wp-content/uploads/2013/02/xml_pretty1.jpg" alt="" width="650" height="278" srcset="/wp-content/uploads/2013/02/xml_pretty1.jpg 650w, /wp-content/uploads/2013/02/xml_pretty1-300x128.jpg 300w" sizes="(max-width: 650px) 100vw, 650px" />][7]

<p style="text-align: center">
  Pretty XML
</p>

## Generic features:

Also I have added many generic functions and generic classes, likely the more outstanding are the autocompletations. Now you can create a code completion model with very few lines:

<pre>import kate
from libkatepate.autocomplete import AbstractJSONFileCodeCompletionModel, reset

class MyCodeCompletionModel(AbstractJSONFileCodeCompletionModel):

    MIMETYPES = [] # List of mimetypes E.G.: ['application/javascript', 'text/html']
    TITLE_AUTOCOMPLETION = "My Auto Completion Model"
    FILE_PATH = 'my_autocompletion.json'
    # Operators to separate the instrunctions
    OPERATORS = ["=", " ", "[", "]", "(", ")", "{", "}", ":", "&gt;", "&lt;",
                 "+", "-", "*", "/", "%", " && ", " || ", ","]

@kate.init
@kate.viewCreated
def createSignalAutocompleteJS(view=None, *args, **kwargs):
    view = view or kate.activeView()
    cci = view.codeCompletionInterface()
    cci.registerCompletionModel(my_code_completion_model)

my_code_completion_model = MyCodeCompletionModel(kate.application)
my_code_completion_model.modelReset.connect(reset)</pre>

The my_autocompletion.json file should have this format:

<pre>{
    "xxx": {
      "category": "module",
      "children": {
        "www": {
          "category": "class",
          "children": {}
        },
        "yyy": {
          "category": "constant",
          "children": {}
        },
        "zzz": {
          "category": "function",
          "args": "(x1, x2, x3)",
          "children": {}
        }
      }
    }
  }</pre>

I encourage you to participate in the KDE project, for me this participation has been very simple because this community is very, very open.

Thanks all,

 [1]: /wp-content/uploads/2013/02/pyplugins.zip "Kate plugins first version"
 [2]: https://github.com/goinnn/Kate-plugins#information "Kate plugins repository"
 [3]: /wp-content/uploads/2013/02/autocompletion_python.jpg
 [4]: /wp-content/uploads/2013/02/autocompletion_python3.jpg
 [5]: /wp-content/uploads/2013/02/jslint1.jpg
 [6]: /wp-content/uploads/2013/02/django_form1.jpg
 [7]: /wp-content/uploads/2013/02/xml_pretty1.jpg