---
title: 'Multiple Windows & Kate on KF5'
author: Christoph Cullmann

date: 2013-12-22T17:16:40+00:00
url: /2013/12/22/multiple-windows-kate-on-kf5/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
In KDE 4, you can open a new window for Kate via &#8220;View -> New Window&#8221;.

This won&#8217;t start a new application instance but just add an other Kate main window to the current instance (which shows the same documents & session & projects).

This is kind of complex to handle internally and we think about dropping that behavior and instead launching just an other Kate application instance if you trigger that menu action.

Pro: Easier code structure, less to maintain, less bugs. Each window is an own process, e.g. if one crashs, not the others die, too.

Contra: You will loose the have the same documents open in two windows with syncing (as the two windows would then be individual processes).

Any opinions about that out there in the lazy web? Feedback is welcome.