---
title: 'Kate on 5: The Future of KTextEditor and Kate Part'
author: Dominik Haumann

date: 2013-11-11T20:02:41+00:00
url: /2013/11/11/kate-on-5-the-future-of-ktexteditor-and-kate-part/
pw_single_layout:
  - "1"
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet
  - python

---
[<img class="alignright  wp-image-2938" title="KDE Frameworks 5 Modularization" src="/wp-content/uploads/2013/11/modularization-270x300.png" alt="" width="146" height="162" srcset="/wp-content/uploads/2013/11/modularization-270x300.png 270w, /wp-content/uploads/2013/11/modularization.png 584w" sizes="(max-width: 146px) 100vw, 146px" />][1]Recently, there was a <a title="KDE Frameworks 5" href="http://dot.kde.org/2013/09/25/frameworks-5" target="_blank">dot story about Frameworks 5</a>: Started in spring of 2011, the KDE software stack is undergoing a heavy split. The idea is to modularize the KDE libraries into lots of rather small units. Each unit has well-defined dependencies, depending on whether it&#8217;s in the tier 1, tier 2, or tier 3 layer, and depending on whether it provides plain functionality, integration, or a solution. If you haven&#8217;t yet, please read the article on the dot for a better understanding.

With this modularization, the question arises about what will happen to the KTextEditor interfaces and its implementation Kate Part – and of course all the applications using Kate Part through the KTextEditor interfaces, namely Kate, KWrite, KDevelop, Kile, RKWard, and all the others&#8230; The purpose of this blog is to give an answer to these questions, and to start a discussion to get feedback.

### A Bit of History ([Funny Read Here][2])

Since not everyone is familiar with &#8220;Kate & friends&#8221; and its sources, let&#8217;s have a quick look at its software architecture:

<img class="aligncenter size-full wp-image-2962" title="Kate's Software Architecture" src="/wp-content/uploads/2013/11/architecture.png" alt="" width="714" height="274" srcset="/wp-content/uploads/2013/11/architecture.png 714w, /wp-content/uploads/2013/11/architecture-300x115.png 300w" sizes="(max-width: 714px) 100vw, 714px" /> 

From this figure, we can see that the software stack has 3 layers: the application layer, the interfaces layer and the backend layer. KTextEditor provides an interface to all functions of an advanced editor component: Documents, Views, etc. (<a title="KTextEditor API Documentation" href="http://api.kde.org/4.11-api/kdelibs-apidocs/interfaces/ktexteditor/html/kte_design.html" target="_blank">see also the API documentation</a>). However, KTextEditor itself does not implement the functionality. So it&#8217;s just a thin layer that is guaranteed to stay compatible for a long time (for the KDE 4 line, KTextEditor is compatible since  2007, so as of now 7 years). This compatibility, or better yet _interface continuity_ is required, since applications in the Application layer use the KTextEditor interfaces to embed a text editor component. The implementation of the text editor itself is completely hidden in Kate Part. Think of a backend library implementing all the KTextEditor interfaces. When an application queries KDE for a KTextEditor component, KDE loads a Kate Part behind the scenes and returns a pointer to the respective KTextEditor classes. This has the advantage, that Kate Part itself can be change without breaking any applications, as long as the interfaces stay the same. Now, with KDE Frameworks 5, this principle will not change. However, interfaces will undergo a huge cleanup, as will be explained now. As a consequence, all nodes that point to or from the KTextEditor node, namely Kate Part on the backend layer as well as applications, will need to adapt to these interfaces.

### Milestone 1: KTextEditor and Kate Part on 5

KTextEditor will be a separate unit in the frameworks split. Therefore, the KTextEditor interfaces will not come bundled with one monolithic &#8216;kdelibs&#8217; as it was the case for that last 10 years. Instead, the KTextEditor interfaces are developed and provided in a separate git repository. This is already now the case: The KTextEditor interfaces exist as a copy in <a title="Kate git repository" href="https://projects.kde.org/projects/kde/applications/kate/repository" target="_blank">Kate&#8217;s git repository</a>, and relevant changes were merged into kdelibs/interfaces/ktexteditor in the KDE 4.x line. For &#8220;KTextEditor on 5,&#8221; the first milestone will be to get KTextEditor compile with the libraries and tools from the frameworks 5 branch. Along with this port, the KTextEditor interfaces have a lot of places that are annotated with &#8220;KDE 5 todos.&#8221; That is, the KTextEditor interfaces will undergo a huge cleanup, providing an even better API for developers than before.

Currently, the KTextEditor and therewith also its implementation Kate Part use the KParts component model. The KParts model allows to easily embed Kate Part in other applications along with Kate Part&#8217;s actions and menus. Further, Kate Part internally uses KIO to load and save files to support network transparent text editing. KParts itself and KIO are both Tier 3 solutions. This implies that KTextEditor along with its implementation Kate Part are a Tier 3 solution.

In other words, a straight port and cleanup of KTextEditor and Kate Part will depend on a lot of high level frameworks. This solution will provide all the features the KTextEditor interfaces provides right now in the KDE SC 4.x line.

Currently, we plan one major change in the KTextEditor on 5: We will remove KTextEditor plugins. Over the last 10 years, we got close to no contributions to the KTextEditor plugins. Existing KTextEditor plugins partly clash with code in Kate Part (for instance the Auto Brackets with the Autobrace plugin), and merging the plugin&#8217;s xml gui into the KTextEditor::Views always requires some hacks to avoid flickering and make it work correctly. Besides, if the KTextEditor plugins are removed, for instance the Kate config dialog only shows one &#8220;Plugins&#8221; item instead of two. This is much cleaner to the user. Existing functionality, like for instance the &#8220;Highlight Selected Text&#8221; plugin, will be included into Kate Part directly. The same holds true for the HTML export feature. This is a bold change. So if you want to discuss this, please write to our mailing list [kwrite-devel@kde.org][3].

The time frame for the KTextEditor port & cleanup is rather short: We want to provide rather stable KTextEditor interfaces so that other applications can rely on it. Therefore, we will probably create a frameworks branch in the Kate git repository in December (<a title="Ongoing Discussion about Kate on 5" href="http://lists.kde.org/?t=138399875900002&r=1&w=4" target="_blank">current proposal on kwrite-devel</a>). Binary and source incompatible changes will be allowed until other applications like KDevelop or Kile are ported to Frameworks 5. Then, the KTextEditor interfaces will again stay binary compatible for years.

### Milestone 2: KWrite and Kate on 5

KWrite is just a thin wrapper around the KTextEditor interfaces and therewith Kate Part. Therefore, KWrite will mostly support just the same functionality as it provides now. The same holds true for Kate. However, Kate itself provides quite a lot of advanced features, for instance to have multiple main windows (View > New Window), or sessions, and a good plugin infrastructure. Of course, Kate itself will also undergo cleanups: i) cleanups due to changes in the KTextEditor interfaces, and ii) cleanups like for instance moving the [Projects plugin][4] Kate itself, making it more easily accessible to other plugins like the Search & Replace or Build plugin. We will also remove support for multiple main windows through &#8220;View > New Window.&#8221; This is due to the fact, that many Kate plugin developers were not aware of this feature, and therefore completely messing up their code by not separating the logic from the view, resulting in crashes or broken behavior when using multiple main windows. Removing the support for multiple main windows, we will loose this feature. However, we get simpler and more maintainable code.

There are other small details that will change. For instance, as it looks right now, the Python pate host plugin in Kate on 5 will only support Python 3 (<a title="Python plugins in Kate" href="http://lists.kde.org/?t=138408441700001&r=1&w=2" target="_blank">current discussion on kwrite-devel</a>). Python developers, you are welcome to contribute here, as always! :-)

### Milestone 3: More Modularization in the KTextEditor Interfaces?

Milestone 1 & milestone 2 will happen rather sooner than later (fixed dates will follow once we&#8217;re sure we can satisfy them). Since the transition to Frameworks 5 allows us to change KTextEditor interfaces, it is the right time to think how we can improve the KTextEditor interfaces and its implementation Kate Part even further. For instance, on the mailing list, the idea was raised to make the KParts model optional. This could be achieved for instance by deriving KTextEditor::Document from QObject directly, and create a thin KParts wrapper, say KTextEditor::DocumentPart that wraps KTextEditor::Document. This would be a major change, though, and possibly require a lot of changes in applications using the KTextEditor interfaces. As of now, it is unclear whether such a solution is feasible.

Another idea was raised at this year&#8217;s <a title="Akademy 2013" href="http://akademy2013.kde.org/" target="_blank">Akademy in Bilbao</a>: Split Kate Part&#8217;s highlighting into a separate library. This way, other applications could use the Kate Part&#8217;s highlighting system. Think of a command line tool to create highlighted html pages, or a syntax highlighter for QTextEdits. The highlighting engine right now is mostly internal to Kate Part, so such a split could happen also later after the initial release of KTextEditor on 5.

### Join Us!

The Kate text editor only exists thanks to all its contributors. Moving to frameworks, it is the perfect time to follow and contribute to the development of Kate. In fact, you can learn a lot (!) in contributing. In case you are interested, have ideas or want to discuss with us, please join our mailing list [kwrite-devel@kde.org][5].

 [1]: /wp-content/uploads/2013/11/modularization.png
 [2]: /2010/08/15/kate-history/ "Kate's History"
 [3]: mailto:kwrite-devel@kde.org "kwrite-devel@kde.org"
 [4]: /2012/11/02/using-the-projects-plugin-in-kate/ "Using the Projects Plugin"
 [5]: mailto:kwrite-devel@kde.org "Kate Mailing List"