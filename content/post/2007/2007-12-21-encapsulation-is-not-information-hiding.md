---
title: Encapsulation is not Information Hiding
author: Dominik Haumann

date: 2007-12-21T13:03:00+00:00
url: /2007/12/21/encapsulation-is-not-information-hiding/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2007/12/encapsulation-is-not-information-hiding.html
categories:
  - Developers

---
As food for thought and in reply to [Why Encapsulation is a Good Thing][1] it&#8217;s interesting to take a closer look. What does encapsulation mean exactly, and what can you do with it? Maybe what you really want is Information Hiding? &#8230;and encapsulation is just a way of possibly achieving it? If you are interested in the details/differences, read the article [Encapsulation is not Information Hiding][2].

 [1]: http://www.kdedevelopers.org/node/3163
 [2]: http://www.javaworld.com/javaworld/jw-05-2001/jw-0518-encapsulation.html