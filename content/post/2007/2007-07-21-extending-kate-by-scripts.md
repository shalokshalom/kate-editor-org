---
title: Extending Kate by Scripts
author: Dominik Haumann

date: 2007-07-21T11:29:00+00:00
url: /2007/07/21/extending-kate-by-scripts/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2007/07/extending-kate-by-scripts.html
categories:
  - Users

---
We [have][1] [seen][2] how scripting basically works for indentation. It&#8217;s also possible to register commandline functions (The command line is bound to F7 by default, or invoke View > Switch to Command Line). We will consider a small example again: sort the selected text.

> <span style="font-family:courier new;">/*&nbsp;kate-script<br />&nbsp;*&nbsp;name:&nbsp;unused<br />&nbsp;*&nbsp;author:&nbsp;foo&nbsp;bar<br />&nbsp;*&nbsp;license:&nbsp;LGPL<br />&nbsp;*&nbsp;version:&nbsp;1<br />&nbsp;*&nbsp;kate-version:&nbsp;3.0<br />&nbsp;*&nbsp;functions:&nbsp;sorter<br />&nbsp;*/</p> 
> 
> <p>
>   function&nbsp;sorter&nbsp;()<br />{<br />&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;(view.hasSelection())&nbsp;{<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var&nbsp;start&nbsp;=&nbsp;view.startOfSelection().line;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var&nbsp;end&nbsp;=&nbsp;view.endOfSelection().line;
> </p>
> 
> <p>
>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var&nbsp;text&nbsp;=&nbsp;document.textRange(start,&nbsp;0,&nbsp;end,&nbsp;document.lineLength(end));
> </p>
> 
> <p>
>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var&nbsp;lines&nbsp;=&nbsp;text.split(&#8220;\n&#8221;);<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lines.sort();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text&nbsp;=&nbsp;lines.join(&#8220;\n&#8221;);
> </p>
> 
> <p>
>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;view.clearSelection();
> </p>
> 
> <p>
>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;document.editBegin();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;document.removeText(start,&nbsp;0,&nbsp;end,&nbsp;document.lineLength(end));<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;document.insertText(start,&nbsp;0,&nbsp;text);<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;document.editEnd();<br />&nbsp;&nbsp;&nbsp;&nbsp;}<br />}</span>
> </p></blockquote> 
> 
> <p>
>   The header line <span style="font-weight: bold;">functions: sorter</span> makes Kate Part aware of the function in the script. A list of functions is supported, separated by white spaces. You can use the function by typing &#8216;sorter&#8217; in the commandline.<br />Some todo items:
> </p>
> 
> <ul>
>   <li>
>     provide better JavaScript API. For example: document.textRange() takes 4 parameters. It would be more elegant to take one range or two cursors, just like we do in the KTextEditor interfaces in kdelibs/interfaces/ktexteditor
>   </li>
>   <li>
>     make is possible to bind scripts to shortcuts. This could be done by e.g. binding commandline functions to shortcuts or implementing a vim-like command-mode in Kate&#8217;s commandline. How to configure the shortcuts is unclear, though.
>   </li>
>   <li>
>     then, think about replacing the C++ implementations of &#8216;uppercase&#8217;, &#8216;lowercase&#8217;, &#8216;capitalize&#8217; etc. with scripts
>   </li>
>   <li>
>     things I forgot&#8230;
>   </li>
> </ul>
> 
> <p>
>   If you are interested subscribe to kwrite-devel@kde.org and contribute :) We also need indentation scripts, of course!
> </p>

 [1]: http://dhaumann.blogspot.com/2007/07/kate-scripting-indentation.html
 [2]: http://dhaumann.blogspot.com/2007/07/kate-more-on-indentation-scripting.html