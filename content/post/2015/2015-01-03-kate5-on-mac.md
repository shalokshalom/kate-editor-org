---
title: Kate5 on Mac
author: Christoph Cullmann

date: 2015-01-03T16:52:44+00:00
url: /2015/01/03/kate5-on-mac/
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Given that the KF5 based Kate works OK on Windows, I would like to get the Mac version up and running, too.

As virtualization of MacOS X is kind of &#8220;forbidden&#8221; and not that nicely usable anyway, as no nice accelerating drivers are available for the standard vm solutions, I just went out into the world and bought some Retina MacBook.

I followed the nice guide on https://github.com/haraldF/homebrew-kf5 (thanks Harald :=) and got some installed Kate/KWrite (after patching kio to skip X11 detection).

But ok, not that usable, at least without any investigation which env vars are missing per default you get a Kate that crashs or just shows the main window central content without any actions/menus/toolbars/sidebars/&#8230;

In addition, the application is scaled, as the manifest that tells we are able to handle retina bla stuff is missing.

Guess lots of work ahead, but given I have now some Mac, I will work on that in the following months ;)

P.S. I never had a Mac before and got often told by &#8220;Mac Fans&#8221; that I should switch to some &#8220;real&#8221; operating system from my stupid borked unusable cluttered hacky &#8230; Linux. Given that the new shiny MacBook directly stalled for ever during the guided &#8220;create your user and give us all your data&#8221; installation wizard on first boot, I am not that impressed ;=) My mother would have been stuck and after some googling that seems to be a &#8220;known issue&#8221;, at least for me, after a hard reboot, the thingy worked and I had a login, more luck than others :P