---
title: 'Akademy 2012 –  We are on the way! (soon)'
author: Christoph Cullmann

date: 2012-02-26T16:54:12+00:00
url: /2012/02/26/akademy-2012-we-are-on-the-way-soon/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users

---
Perhaps a bit early, but typical german, me and Dominik booked our flights to Tallinn.

Lets hope we will have a lot fun there and meet old and new friends ;)

See you all, at Tallinn.