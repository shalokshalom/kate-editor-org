---
title: 'RFC: Data Recovery'
author: Dominik Haumann

date: 2012-07-08T19:15:25+00:00
url: /2012/07/08/rfc-data-recovery/
pw_single_layout:
  - "2"
categories:
  - Developers
  - Users
tags:
  - planet

---
Currently, we use our own passive notification bar to show a recovery bar when a swap file was found:

<img class="aligncenter size-full wp-image-1897" title="Data Recovery Widget" src="/wp-content/uploads/2012/07/recovery11.png" alt="" width="609" height="332" srcset="/wp-content/uploads/2012/07/recovery11.png 609w, /wp-content/uploads/2012/07/recovery11-300x163.png 300w" sizes="(max-width: 609px) 100vw, 609px" /> 

Since version 4.7 we have a class called <a title="KMessageWidget Demo" href="http://agateau.com/2011/04/21/kde-ux-2011/" target="_blank">KMessageWidget</a> (<a title="KMessageWidget - Api Documentation" href="http://api.kde.org/4.x-api/kdelibs-apidocs/kdeui/html/classKMessageWidget.html" target="_blank">api documentation</a>). Using KMessageWidget, the notification could also look like this:

<img class="aligncenter size-full wp-image-1898" title="Data Recovery Widget" src="/wp-content/uploads/2012/07/recovery2.png" alt="" width="816" height="332" srcset="/wp-content/uploads/2012/07/recovery2.png 816w, /wp-content/uploads/2012/07/recovery2-300x122.png 300w" sizes="(max-width: 816px) 100vw, 816px" /> 

Imo, the new one looks nicer, as it much better distinguishes the notification popup from the rest of the ui. However, it several drawbacks:

  * the minimum width is now 800 pixel due to the label (too large, might break layout in apps)
  * the title &#8220;Data Recovery&#8221; was omitted
  * the &#8220;Help&#8221; link, showing a tool tip with further information, can not be added anymore (the api does not allow it)

Possible solutions:

  * put the buttons under the label, as it was in the old version (currently, the api does not allow it, so we&#8217;d need something like setButtonsUnderText(bool))
  * wrap the text in the label (looks aweful, since the buttons appear then between two lines of text)
  * to get the &#8220;Help&#8221; label back, the api would need to forward the QLabel&#8217;s signal <a title="QLabel::linkActivated()" href="http://qt-project.org/doc/qt-4.8/qlabel.html#linkActivated" target="_blank">linkActivated(const QString&)</a>

I&#8217;m not sure whether the API can be extended in the 4.x line, though&#8230;

Comments?