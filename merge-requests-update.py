#!/usr/bin/env python3

import os
import re
import sys
import yaml
import argparse
import gitlab
import hashlib
import datetime
import html
import subprocess

# merge requests dir
try:
    os.mkdir("content/merge-requests/")
except FileExistsError:
    pass

# create merge-requests mark down file
requestsMd = open(sys.path[0] + "/content/merge-requests/_index.md", 'w')
requestsMd.write('''---
title: Merge Requests
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
menu:
  main:
    weight: 100
    parent: menu
---

This pages provides an overview of the merge requests we are current working on or did already accepted for the Kate, KTextEditor, KSyntaxHighlighting and kate-editor.org repositories.

''')

# get all merge requests, output open stuff directory, hash merged ones by year + month
veryFirst = 1
datetoMerge = {}
repos = { "utilities/kate": "Kate", "frameworks/ktexteditor": "KTextEditor", "frameworks/syntax-highlighting": "KSyntaxHighlighting", "websites/kate-editor-org": "kate-editor.org" }
merged = {}
with gitlab.Gitlab( "https://invent.kde.org", ssl_verify=True ) as gl:
    for path in repos:
        # get project
        project = gl.projects.get(path)

        # get open stuff for direct output
        firstOne = 1
        for request in project.mergerequests.list(state='opened', order_by='updated_at', sort='desc', all=True):
            if veryFirst == 1:
                veryFirst = 0
                requestsMd.write("## Currently Open Merge Requests\n")
                requestsMd.write("Our team is still working on the following requests. Feel free to take a look and help out, if any of them is interesting for you!\n")

            if firstOne == 1:
                firstOne = 0
                requestsMd.write("### {}\n\n".format(repos[path]))

            requestsMd.write("- **[{}]({})**<br />\n".format(html.escape(request.title), request.web_url))
            requestsMd.write("Request authored by [{}]({}).\n\n".format(request.author['name'], request.author['web_url']))

        # get merged stuff
        for request in project.mergerequests.list(state='merged', order_by='updated_at', sort='desc', all=True):
            # merge + creation day, we want to compute how long it took

            # seems to be undefined in some cases :/ => use updated_at for these cases
            try:
                mergeDate = datetime.date.fromisoformat(request.merged_at[0:10])
            except TypeError:
                mergeDate = datetime.date.fromisoformat(request.updated_at[0:10])

            createDate = datetime.date.fromisoformat(request.created_at[0:10])

            # nice duration
            daysTaken = (mergeDate - createDate).days
            if daysTaken == 0:
                daysTaken = "at creation day"
            elif daysTaken == 1:
                daysTaken = "after one day"
            elif daysTaken > 1:
                daysTaken = "after {} days".format(daysTaken)


            # create nice markdown variant of the request's major infos
            markdownDescription = ""
            markdownDescription += "- **{} - [{}]({})**<br />\n".format(repos[path], html.escape(request.title), request.web_url)
            markdownDescription += "Request authored by [{}]({}) and merged {}.\n\n".format(request.author['name'], request.author['web_url'], daysTaken)

            # store with year -> month -> list of requests pre-formatted for output
            datetoMerge.setdefault(mergeDate.year, {}).setdefault(mergeDate.month, {}).setdefault(mergeDate.day, []).append(markdownDescription)

            # remember how many things got merged for overall stats
            merged.setdefault(path, {}).setdefault(0, 0)
            merged.setdefault(path, {}).setdefault(mergeDate.year, 0)
            merged[path][0] += 1
            merged[path][mergeDate.year] += 1

# overall merges
requestsMd.write("\n## Overall Accepted Merge Requests\n")
for path in repos:
    requestsMd.write("- {} patches for [{}](https://invent.kde.org/{}/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)\n".format(merged[path][0], repos[path], path))
requestsMd.write("\n")

# output a nice markdown table for each year & month with all merged requests
for year in sorted(datetoMerge, reverse=True):
    requestsMd.write("\n\n## Accepted Merge Requests of {}\n\n".format(year))

    # merges of the given year
    for path in repos:
        if year in merged[path]:
            requestsMd.write("- {} patches for [{}](https://invent.kde.org/{}/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)\n".format(merged[path][year], repos[path], path))
    requestsMd.write("\n")

    requestsMd.write("\n\n### Monthly Statistics\n\n")

    # output per month and there per week of year, very useful to writeup blog posts about progress
    for month in sorted(datetoMerge[year], reverse=True):
        seenweeks = {}
        nicedate = datetime.date(year, month, 1).strftime('%B %Y')
        monthpage = datetime.date(year, month, 1).strftime('%Y/%m')

        # new extra page per month
        try:
            os.mkdir("content/merge-requests/{}".format(year))
        except FileExistsError:
            pass
        try:
            os.mkdir("content/merge-requests/{}".format(monthpage))
        except FileExistsError:
            pass

        requestsMdMonth = open(sys.path[0] + "/content/merge-requests/{}/_index.md".format(monthpage), 'w')
        requestsMdMonth.write('''---
title: Merge Requests - {}
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/{}/
---

'''.format(nicedate, monthpage))

        # we will only output the merge request of the latest month on the toplevel page
        requests = 0
        for day in sorted(datetoMerge[year][month], reverse=True):
            currentweek = datetime.date(year, month, day).strftime('%V')
            if not currentweek in seenweeks:
                requestsMdMonth.write("#### Week {}\n\n".format(currentweek))
                seenweeks[currentweek] = 1
            for request in datetoMerge[year][month][day]:
                requests += 1
                requestsMdMonth.write(request)

        # ensure month file is closed properly before git process uses it
        requestsMdMonth.close()

        # provide link to sub-page
        requestsMd.write("* [{}](/merge-requests/{}/) ({} requests)\n".format(nicedate, monthpage, requests))

# close all files before git runs
requestsMd.close()

# add new stuff to git
subprocess.run(["git", "add", "content/merge-requests/"])
